#ifndef CONFIG_H_
#define CONFIG_H_

#define _XOPEN_SOURCE	600
#define _GNU_SOURCE


#define HOST		"localhost"
#define PORT		"9000"
#define BACKLOG		10
#define MAX_EVENTS	200

#endif	// CONFIG_H_