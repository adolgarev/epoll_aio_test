#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <errno.h>
#include <libaio.h>

#include "log.h"
#include "aio.h"


typedef struct aio_cb_s_ {
	int fd;
	aio_cb fun;
	aio_data_u data;
} aio_cb_s;

static int epollfd = -1;
static io_context_t ioctx;
static int afd = -1;

static int eventfd();
static aio_err_e on_io_event(aio_data_u data, uint32_t events);
//-------------------------------------------------------------------

int aioinit() {
	if ((epollfd = epoll_create(2000)) == -1) {
		log_syserr("epoll_create");
		goto err_end;
	}
	
	if (io_setup(100, &ioctx)) {
		log_err("io_setup");
		goto err_end;
	}
	
	// use with epoll
	/*if ((afd = eventfd(0, EFD_NONBLOCK)) == -1) {
		log_syserr("eventfd");
		goto err_end;
	}*/
	if ((afd = eventfd()) == -1)
		goto err_end;
	
	if (aio_add_in_et(afd, on_io_event, (aio_data_u){ .ptr=NULL }))
		goto err_end;
	
	return 0;
err_end:
	aiodel();
	return 1;
}
//-------------------------------------------------------------------

int aiodel() {
	if (epollfd != -1)
		close(epollfd);
	if (afd != -1)
		close(afd);
	io_destroy(ioctx);
	return 0;
}
//-------------------------------------------------------------------

int aioloop() {

	for (;;) {
		struct epoll_event events[MAX_EVENTS];
		int nfds, n;
		
		if ((nfds = epoll_wait(epollfd, events, MAX_EVENTS, -1)) == -1) {
			if (errno == EINTR)
				continue;
			log_syserr("epoll_wait");
			goto err_end;
		}

		for (n = 0; n < nfds; n++) {
			int ret = 0;
			aio_cb_s *p = events[n].data.ptr;
			ret = p->fun(p->data, events[n].events);
			if (ret & (aio_err_close | aio_err)) {
				// will be removed from epoll interests list
				close(p->fd);
				free(p);
				continue;
			}
			if (ret & aio_err_again) {
				// TODO: add to pending list
			}
			if (ret & aio_err_write) {
				if (!(events[n].events & EPOLLOUT)) {
					// add EPOLLOUT if wasn't added before
					struct epoll_event ev;
					ev.events = EPOLLIN | EPOLLOUT;
					ev.data.ptr = p;
					if (epoll_ctl(epollfd, EPOLL_CTL_MOD, p->fd, &ev) == -1) {
						log_syserr("epoll_ctl");
						goto err_end;
					}
				}
			}
			else if (events[n].events & EPOLLOUT) {
				// remove EPOLLOUT if was added before
				struct epoll_event ev;
				ev.events = EPOLLIN;
				ev.data.ptr = p;
				if (epoll_ctl(epollfd, EPOLL_CTL_MOD, p->fd, &ev) == -1) {
					log_syserr("epoll_ctl");
					goto err_end;
				}
			}
		}
	}
	
	return 0;
err_end:
	return 1;
}
//-------------------------------------------------------------------

/* for x86_64 */
#define SYS_eventfd       284
int eventfd() {
	int fd = -1;
	
	if ((fd = syscall(SYS_eventfd, 0)) == -1) {
		log_syserr("SYS_eventfd");
		goto err_end;
	}
	
	if (setnonblocking(fd))
		goto err_end;
	
	return fd;
err_end:
	if (fd != -1)
		close(fd);
	return -1;
}
//-------------------------------------------------------------------

int setnonblocking(int fd) {
	int flags;
	
	if ((flags = fcntl(fd, F_GETFL, 0)) == -1) {
		log_syserr("fcntl");
		return -1;
	}
	if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1) {
		log_syserr("fcntl");
		return -1;
	}
	
	return 0;
}
//-------------------------------------------------------------------

int aio_add_in(int fd, aio_cb fun, aio_data_u data) {
	aio_cb_s *p = NULL;
	struct epoll_event ev;
	
	p = malloc(sizeof(aio_cb_s));
	if (!p) {
		log_syserr("malloc");
		goto err_end;
	}
	
	memset(p, 0, sizeof(aio_cb_s));
	p->fd = fd;
	p->fun = fun;
	p->data = data;
	
	ev.events = EPOLLIN;
	ev.data.ptr = p;
	if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev) == -1) {
		log_syserr("epoll_ctl");
		goto err_end;
	}
	
	return 0;
err_end:
	if (p)
		free(p);
	return 1;
}
//-------------------------------------------------------------------

int aio_add_in_et(int fd, aio_cb fun, aio_data_u data) {
	aio_cb_s *p = NULL;
	struct epoll_event ev;
	
	p = malloc(sizeof(aio_cb_s));
	if (!p) {
		log_syserr("malloc");
		goto err_end;
	}
		
	memset(p, 0, sizeof(aio_cb_s));
	p->fd = fd;
	p->fun = fun;
	p->data = data;
	
	ev.events = EPOLLIN | EPOLLET;
	ev.data.ptr = p;
	if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev) == -1) {
		log_syserr("epoll_ctl");
		goto err_end;
	}
	
	return 0;
err_end:
	if (p)
		free(p);
	return 1;
}
//-------------------------------------------------------------------

aio_err_e on_io_event(aio_data_u data, uint32_t events) {
	int ret = 0;

	for (;;) {
		u_int64_t eval = 0;
		
		ret = read(afd, &eval, sizeof(eval));
		if (ret != sizeof(eval)) {
			if (ret == -1 && errno == EAGAIN)
				break;
			
			log_syserr("read");
			goto err_end;
		}

		// eval equals to the amount of ready events,
		// TODO get bulk of events at once via io_getevents
		while (eval > 0) {
			aio_cb_s *p;
			struct io_event event;
			if (io_getevents(ioctx, 1, 1, &event, NULL) != 1) {
				log_err("io_getevents");
				goto err_end;
			}
			
			p = event.data;
			p->fun(p->data, 0);

			eval--;
		}
	}

	return aio_ok;
err_end:
	return aio_err;
}
//-------------------------------------------------------------------

int aio_submit_read(int fd, void *buf, size_t count, long long offset, aio_cb fun, aio_data_u data) {
	int ret = 0;
	aio_cb_s *p = NULL;
	struct iocb iocb;
	struct iocb* iocbs = &iocb;
	
	p = malloc(sizeof(aio_cb_s));
	if (!p) {
		log_syserr("malloc");
		goto err_end;
	}
		
	memset(p, 0, sizeof(aio_cb_s));
	p->fd = fd;
	p->fun = fun;
	p->data = data;

	io_prep_pread(&iocb, fd, buf, count, offset);
	iocb.data = p;
	// use with epoll
	io_set_eventfd(&iocb, afd);
	ret = io_submit(ioctx, 1, &iocbs);
	switch (ret) {
		case -EAGAIN:
			log_err("io_submit: EAGAIN");
			break;
		case -EBADF:
			log_err("io_submit: EBADF");
			break;
		case -EFAULT:
			log_err("io_submit: EFAULT");
			break;
		case -EINVAL:
			log_err("io_submit: EINVAL");
			break;
		case -ENOSYS:
			log_err("io_submit: ENOSYS");
			break;
		default:
			if (ret < 0)
				log_err("io_submit");
			break;
	}
	if (ret != 1)
		goto err_end;
	
	return 0;
err_end:
	if (p)
		free(p);
	return 1;
}
//-------------------------------------------------------------------
