.PHONY: clean all

CC=gcc
CFLAGS=-std=c99 -g -Wall
LIBS=-laio
BIN=test
HEADERS=$(wildcard *.h)
OBJECTS=$(patsubst %.c,%.o,$(wildcard *.c))

all: $(BIN)

test: aio.o  test.o
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $<

clean:
	rm -f $(BIN) *~ *.o
