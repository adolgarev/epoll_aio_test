#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>
#include <errno.h>

#include "log.h"
#include "aio.h"




static int listen_sock_create();
static aio_err_e on_accept(aio_data_u data, uint32_t events);
static aio_err_e on_socket(aio_data_u data, uint32_t events);
static aio_err_e on_testfd(aio_data_u data, uint32_t events);
//-------------------------------------------------------------------

int main(int argc, const char* argv[]) {
	int listen_sock = -1;
	int ret = 0;
	void *buffer = NULL;
	int test_fd = -1;
	
	if (argc < 2)
		goto err_end;
	
	if (aioinit())
		goto err_end;
	
	if ((listen_sock = listen_sock_create()) == -1)
		goto err_end;
	
	if (aio_add_in(listen_sock, on_accept, (aio_data_u){ .fd=listen_sock }))
		goto err_end;
	
	
	{
		test_fd = open(argv[1], O_RDONLY | O_DIRECT);
		
		long sz = sysconf(_SC_PAGESIZE);
		ret = posix_memalign(&buffer, sz, sz * 4);
		switch (ret) {
			case EINVAL:
				log_err("posix_memalign: The alignment argument was not a power of two, or was not a multiple of sizeof(void *).");
				goto err_end;
			case ENOMEM:
				log_err("posix_memalign: There was insufficient memory to fulfill the allocation request.");
				goto err_end;
			case 0:
				break;
			default:
				log_err("posix_memalign");
				goto err_end;
		}
		
		if (aio_submit_read(test_fd, buffer, sz * 4, 0, on_testfd, (aio_data_u){ .ptr=NULL }))
			goto err_end;
	}
	
	
	if (aioloop())
		goto err_end;
	
	
	close(test_fd);
	free(buffer);
	close(listen_sock);
	aiodel();
	return 0;
err_end:
	if (test_fd != -1)
		close(test_fd);
	free(buffer);
	if (listen_sock != -1)
		close(listen_sock);
	aiodel();
	return 1;
}
//-------------------------------------------------------------------

int listen_sock_create() {
	int sockfd = 0;
	int yes = 1;
	int ret = 0;
	struct addrinfo hints;
	struct addrinfo *servinfo = NULL, *p = NULL;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	if ((ret = getaddrinfo(HOST, PORT, &hints, &servinfo)) != 0) {
		log_err("getaddrinfo: %s", gai_strerror(ret));
		goto err_end;
	}
	
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			log_syserr("socket");
			goto err_loop;
		}

		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
			log_syserr("setsockopt");
			goto err_end;
		}

		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			log_syserr("socket");
			goto err_loop;
		}

		break;
	err_loop:
		if (sockfd != -1)
			close(sockfd);
	}
	
	if (p == NULL)  {
		log_err("failed to bind");
		goto err_end;
	}

	freeaddrinfo(servinfo);
	servinfo = NULL;
	
	if (listen(sockfd, BACKLOG) == -1) {
		log_syserr("listen");
		goto err_end;
	}
	
	
	return sockfd;
err_end:
	if (servinfo)
		freeaddrinfo(servinfo);
	return -1;
}
//-------------------------------------------------------------------

aio_err_e on_accept(aio_data_u data, uint32_t events) {
	int listen_sock = -1;
	int conn_sock = -1;
	
	listen_sock = data.fd;
	
	if ((conn_sock = accept(listen_sock, NULL, NULL)) == -1) {
		log_syserr("accept");
		goto err_end;
	}
	if (setnonblocking(conn_sock))
		goto err_end;
	
	if (aio_add_in_et(conn_sock, on_socket, (aio_data_u){ .fd=conn_sock }))
		goto err_end;
	
	return aio_ok;
err_end:
	if (conn_sock != -1)
		close(conn_sock);
	return aio_err;
}
//-------------------------------------------------------------------

aio_err_e on_socket(aio_data_u data, uint32_t events) {
	int fd;
	int nbytes;
	char buf[256];
	
	fd = data.fd;
	
	if (events & (aio_ev_err | aio_ev_hup))
		return aio_err_close;
	
	if (events & aio_ev_in) {
		for (;;) {
			if ((nbytes = recv(fd, buf, sizeof(buf) - 1, 0)) <= 0) {
				if (nbytes == -1 && (errno == EAGAIN || errno == EWOULDBLOCK))
					break;
				else if (nbytes == -1)
					log_syserr("recv");
				else if (nbytes == 0)
					log_debug("%d closed from other peer", fd);
				return aio_err_close;
			}
			else {
				// we got some data from a client
				buf[nbytes] = '\0';
				log_debug("from %d: %s", fd, buf);
			}
		}
	}
	
	if (events & aio_ev_out) {
	}
	
	return aio_ok;
}
//-------------------------------------------------------------------

aio_err_e on_testfd(aio_data_u data, uint32_t events) {
	log_debug("aio read success");
	return aio_err_close;
}
//-------------------------------------------------------------------
