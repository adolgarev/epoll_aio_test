#ifndef AIO_H_
#define AIO_H_

#include <stdint.h>
#include <sys/epoll.h>

typedef union aio_data_u_ {
	void *ptr;
	int fd;
	uint32_t u32;
	uint64_t u64;
} aio_data_u;

typedef enum aio_err_e_ {
	aio_ok = 0,
	aio_err = 1,
	aio_err_again = 2,	// add to pending list
	aio_err_close = 4,	// close socket
	aio_err_write = 8	// if write blocks listen to aio_ev_out
} aio_err_e;

typedef enum aio_ev_e_ {
	aio_ev_in = EPOLLIN,
	aio_ev_out = EPOLLOUT,
	aio_ev_err = EPOLLERR,
	aio_ev_hup = EPOLLHUP
} aio_ev_e;


typedef aio_err_e (*aio_cb)(aio_data_u data, uint32_t events);



int aioinit();
int aiodel();
int aioloop();
int setnonblocking(int fd);
int aio_add_in(int fd, aio_cb fun, aio_data_u data);
int aio_add_in_et(int fd, aio_cb fun, aio_data_u data);
int aio_submit_read(int fd, void *buf, size_t count, long long offset, aio_cb fun, aio_data_u data);


#endif	// AIO_H_